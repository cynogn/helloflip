package com.androidhive.xmlparsing;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Customer Handler for the parser
 * 
 * @author gautam
 * 
 */
public class PicsHandler extends DefaultHandler {

	private boolean id = false;
	private boolean imageUrl = false;
	private List<PicsDetails> picturesDetailsList = new ArrayList<PicsDetails>();
	private PicsDetails picDetails = new PicsDetails();

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub

		if (qName.equalsIgnoreCase("ID")) {
			id = true;
		}
		if (qName.equalsIgnoreCase("IMAGEURL")) {
			imageUrl = true;
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		if (id) {
			picDetails.setId(new String(ch, start, length));
			id = false;
		}

		if (imageUrl) {
			picDetails.setImageUrl(new String(ch, start, length));
			getPicturesDetailsList().add(picDetails);
			picDetails = new PicsDetails();
			imageUrl = false;
		}

	}

	public List<PicsDetails> getPicturesDetailsList() {
		return picturesDetailsList;
	}

	public void setPicturesDetailsList(List<PicsDetails> picturesDetailsList) {
		this.picturesDetailsList = picturesDetailsList;
	}

}
