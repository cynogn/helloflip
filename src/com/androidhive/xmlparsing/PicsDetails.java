package com.androidhive.xmlparsing;

/**
 * PicsDetails contains the Details of the pics
 * 
 * @author gautam
 * 
 */
public class PicsDetails {
	private String mId;
	private String mImageUrl;

	/**
	 * getter of mid
	 * 
	 * @return
	 */
	public String getId() {
		return mId;
	}

	/**
	 * Setter of mId
	 * 
	 * @param string
	 */
	public void setId(String string) {
		this.mId = string;
	}

	/**
	 * Getter of the mImageUrl
	 * 
	 * @return
	 */
	public String getImageUrl() {
		return mImageUrl;
	}

	/**
	 * Setter of the mImageUrl
	 * 
	 * @param imageUrl
	 */
	public void setImageUrl(String imageUrl) {
		this.mImageUrl = imageUrl;
	}
}
