package com.gautam.helloflip;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.androidhive.xmlparsing.PicsDetails;
import com.androidhive.xmlparsing.PicsHandler;
import com.aphidmobile.flip.FlipViewController;

/**
 * MainActivity
 * 
 * @author gautam
 * 
 */
public class MainActivity extends Activity {
	private List<PicsDetails> picsList = new ArrayList<PicsDetails>();
	private FlipViewController flipView;
	/**
	 * Gets loaded from Library
	 */
	;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		flipView = new FlipViewController(MainActivity.this);
		new ParseAyncTask().execute();
	}

	@Override
	protected void onResume() {
		super.onResume();
		flipView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		flipView.onPause();
	}

	List<PicsDetails> loadParser() {
		AssetManager asserManager = null;
		InputStream inputStream = null;
		PicsHandler handler = new PicsHandler();
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			asserManager = this.getAssets();
			inputStream = asserManager.open("sampleXml.xml");
			saxParser.parse(inputStream, handler);

		} catch (Exception e) {

		}
		return handler.getPicturesDetailsList();

	}

	/**
	 * Parsing the XML
	 * 
	 * @author gautam
	 * 
	 */
	class ParseAyncTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			picsList = loadParser();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			flipView.setAdapter(new MyBaseAdapter(MainActivity.this, flipView,
					picsList));
			setContentView(flipView);
			super.onPostExecute(result);
		}
	}
}
