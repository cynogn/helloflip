package com.gautam.helloflip;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidhive.xmlparsing.PicsDetails;
import com.aphidmobile.flip.FlipViewController;
import com.aphidmobile.utils.AphidLog;

/**
 * Adapter of the flipView
 * 
 * @author gautam
 * 
 */
class MyBaseAdapter extends BaseAdapter {

	private FlipViewController controller;

	private Context context;

	private LayoutInflater inflater;

	private Bitmap placeholderBitmap;
	ArrayList<PicsDetails> pics = new ArrayList<PicsDetails>();

	MyBaseAdapter(Context context, FlipViewController controller,
			List<PicsDetails> picsList) {
		inflater = LayoutInflater.from(context);
		this.context = context;
		this.controller = controller;
		this.pics.addAll(picsList);
		placeholderBitmap = BitmapFactory.decodeResource(
				context.getResources(), android.R.drawable.dark_header);
	}

	@Override
	public int getCount() {
		return pics.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View layout = convertView;
		if (convertView == null)
			layout = inflater.inflate(R.layout.flipboardui, null);
		System.out.println("POSITION" + position);
		PicsDetails pic = pics.get(position);

		UI.<TextView> findViewById(layout, R.id.title).setText(
				AphidLog.format("%d. %s", position + 1, pic.getId()));

		UI.<TextView> findViewById(layout, R.id.description).setText(
				Html.fromHtml(pic.getImageUrl()));

		ImageView photoView = UI.findViewById(layout, R.id.photo);
		// Use an async task to load the bitmap
		boolean needReload = true;
		AsyncImageTask previousTask = AsyncDrawable.getTask(photoView);
		if (previousTask != null) {
			if (previousTask.getPageIndex() == position
					&& previousTask.getImageName().equals(pic.getImageUrl()))
				needReload = false;
			else
				previousTask.cancel(true);
		}

		if (needReload) {
			AsyncImageTask task = new AsyncImageTask(photoView, controller,
					position, pic.getImageUrl());
			photoView.setImageDrawable(new AsyncDrawable(
					context.getResources(), placeholderBitmap, task));

			task.execute();
		}

		return layout;
	}
}

/**
 * Borrowed from the official BitmapFun tutorial:
 * http://developer.android.com/training/displaying-bitmaps/index.html
 */
final class AsyncDrawable extends BitmapDrawable {
	private final WeakReference<AsyncImageTask> taskRef;

	public AsyncDrawable(Resources res, Bitmap bitmap, AsyncImageTask task) {
		super(res, bitmap);
		this.taskRef = new WeakReference<AsyncImageTask>(task);
	}

	public static AsyncImageTask getTask(ImageView imageView) {
		Drawable drawable = imageView.getDrawable();
		if (drawable instanceof AsyncDrawable)
			return ((AsyncDrawable) drawable).taskRef.get();

		return null;
	}
}

final class AsyncImageTask extends AsyncTask<Void, Void, Bitmap> {

	private final WeakReference<ImageView> imageViewRef;
	private final WeakReference<FlipViewController> controllerRef;
	private final int pageIndex;
	private final String imageName;

	public AsyncImageTask(ImageView imageView, FlipViewController controller,
			int pageIndex, String imageName) {
		imageViewRef = new WeakReference<ImageView>(imageView);
		controllerRef = new WeakReference<FlipViewController>(controller);
		this.pageIndex = pageIndex;
		this.imageName = imageName;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public String getImageName() {
		return imageName;
	}

	@Override
	protected Bitmap doInBackground(Void... params) {
		String urldisplay = imageName;
		Bitmap mIcon11 = null;
		try {
			InputStream in = new java.net.URL(urldisplay).openStream();
			mIcon11 = BitmapFactory.decodeStream(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mIcon11;
	}

	@Override
	protected void onPostExecute(Bitmap bitmap) {
		if (isCancelled())
			return;

		ImageView imageView = imageViewRef.get();
		if (imageView != null && AsyncDrawable.getTask(imageView) == this) {
			imageView.setImageBitmap(bitmap);
			FlipViewController controller = controllerRef.get();
			if (controller != null)
				controller.refreshPage(pageIndex);
		}
	}
}
